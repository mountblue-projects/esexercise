// 1. Use the let keyword in an example function


function example1(data) {
    let i = data;
    return i * 2;
}
console.log(example1(4));

// 2. Use the const keyword in an example function

function example2(data) {
    const i = data * 2;
    return i;
}
console.log(example2(2));

// 3. Create an arrow function that finds the square of a number

const square = a => a * a;

console.log(square(5));

// 4. Create an arrow function that adds two numbers

const add = (a, b) => a + b;

console.log(add(5, 4));

// 5. Create a multi-line string and then split the string into the corresponding lines and print the lines

const st = `Quis sint dolore ad esse adipisicing aliquip eu. Exercitation ipsum exercitation deserunt commodo eu reprehenderit tempor incididunt officia. Aute quis amet ad esse mollit adipisicing magna velit amet ut laboris. Quis quis laborum voluptate aliqua labore elit culpa quis quis irure esse id cillum sunt.`
const lines = st.split('. ');

for (const line of lines) {
    console.log(line);
}

// 6. Create a function that calculates the area of a circle. If the radius of the circle is not provided assume that the default radius is 5. Use the JavaScript default parameter feature to implement the function

function areaOfCircle(radius = 5) {
    return (Math.PI * radius * radius);
}
console.log(areaOfCircle(7));

console.log(areaOfCircle());


// 7. Create a function that calculates the area of a circle. If the radius of the circle is not provided assume that the default radius is 5. Use the JavaScript default parameter feature to implement the function

let person = {
    name: 'Harry Potter',
    location: 'London'
}
const str = `${person.name} is located in ${person.location}`;
console.log(str);

// using template literals

// 8. Show an example where an array is destructured using destructuring assignment

// 1
let a, b, demo;
[a, b] = [10, 20];
console.log(a);
console.log(b);
// 2
[a, b, , demo] = [10, 20, 30, 40, 50];
console.log(a, b, demo);


// 9. Show an example where an object is destructured using destructuring assignment in the function body

let dog = {
    breed: "huskey",
    color: "grey"
}
let fish = {
    does: "swim"
}

function sampleObjDestructuring(obj1, obj2) {
    // 1
    let breed, color;
    ({ breed, color } = obj1);
    console.log(`${breed},${color}`);

    // 2
    let newObj = { ...obj1, ...obj2 };
    console.log(newObj);

}
sampleObjDestructuring(dog, fish);

// 10. Show an example where a function argument which is an object is destructured inside the parantheses of the function

let host = {
    firstName: "ayush",
    lastName: "k"
}

function sampleObjDestructuring2({ firstName, lastName }) {
    console.log(firstName, lastName);
}

sampleObjDestructuring2(host);

// 11. Show an example where enhanced object literals is used.

const animal = "panda";
const eats = "bamboo";
// enhanced practice of object literals
const animalObject = { animal, eats };

console.log(animalObject);

// 12. Create a function sum that takes any number of numbers as arguments and calculates the sum of the input numbers using the rest parameter syntax
function sum(...argument) {
    let s = 0;
    for (const ele of argument) {
        s += ele;
    }
    return (s);
}

console.log(sum(1, 2, 5, 6));

// 13. Use the spread syntax to expand an array of numbers and pass the elements of the array as arguments to the sum function created in the previous example

const arrayOfNumbers = [4, 5, 6];
console.log(sum(...arrayOfNumbers));

// 14. Use the for..of loop to iterate through all values in an array

const sampleArray = [50, 30, 44];
for (const element of sampleArray) {
    console.log(element);
}

// 15. Iterate through all keys of an object using Object.keys

const sampleObject = {
    name: "ayush",
    lastName: "k",
    age: 19,
    branch: "cse"
}

let keysArray = Object.keys(sampleObject);
console.log(keysArray);

// 16. Iterate through all values of an object using Object.values

let valuesArray = Object.values(sampleObject);
console.log(valuesArray);

// 17. Iterate through all the key / value pairs of an object using Object.entries

for (const [key, value] of Object.entries(sampleObject)) {
    console.log(`${key} : ${value}`);
}